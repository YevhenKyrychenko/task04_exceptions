package com.kyrychenko;

public class CustomResource implements AutoCloseable {
    public void close() throws Exception {
        throw new CustomException();
    }

    public void openResource() {
        System.out.println("Resource is opened");
    }
}
