package com.kyrychenko;

public class Main {
    public static void main(String[] args) throws Exception {
        try (CustomResource resource = new CustomResource()) {
            resource.openResource();
        }
    }
}
